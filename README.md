## Tổ chức folder

```
src
|
|__ components (shared components)
|
|__ features
    |__ Article
    |   |__ components
    |   |   |__ ArticleCard
    |   |   |__ ArticleComment
    |   |   |__ ArticleForm
    |   |   |__ ArticleList
    |   |   |__ ArticleTagList
    |   |
    |   |__ pages
    |       |__ ArticleAddEdit
    |       |__ ArticleDetailPage
    |       |__ ArticleHomePage
    |
    |__ User
        |__ components
        |  |__ UserFollow
        |  |__ UserForm
        |  |__ UserInformation
        |
        |__ pages
            |__ UserFavites
            |__ UserProfile
            |__ UserSetting
    
```