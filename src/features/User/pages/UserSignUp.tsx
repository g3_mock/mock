import { Grid } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { SignUpFormValues } from "../../../models/user.model";
import { signUpAction } from "../../../redux/actionCreaters/user.action";
import SignUpForm from "../components/SignUpForm";
import styles from "./userSignInOrSignUp.module.css";

const UserSignUp = () => {
  const isAuthenticated = useSelector(
    (state: any) => state.user.isAuthenticated
  );

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (isAuthenticated) {
      history.push("/");
    }
  }, [isAuthenticated, history]);

  const handleSubmit = (values: SignUpFormValues) => {
    dispatch(signUpAction(values));
  };

  return (
    <div className={`${styles.background} bg-light`}>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={10}>
          <div className={styles.form}>
            <Grid container>
              <Grid item xs={12} sm={6}>
                <div className={styles.image}></div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <div className="pt-5 px-5">
                  <h2 className="font-weight-bold text-center m-0">Sign up</h2>
                  <SignUpForm onSubmit={handleSubmit} />
                  <p className="mt-3 text-sm-center text-muted font-weight-light">
                    Have already an account?
                    <Link className="font-weight-bold" to="/login">
                      {" "}
                      Sign in here
                    </Link>
                  </p>
                  <p className="mt-3 text-sm-center text-muted font-weight-light">
                    Go to
                    <Link className="font-weight-bold" to="/">
                      {" "}
                      home page
                    </Link>
                    .
                  </p>
                </div>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default UserSignUp;
