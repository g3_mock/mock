import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { RootState } from "../../../models/redux.model";
import { UserFormValues } from "../../../models/user.model";
import { updateUser } from "../../../redux/actionCreaters/user.action";
import { UserSettingForm } from "../components/UserSettingForm";

const UserSetting = () => {
  const currentUser = useSelector((state: RootState) => state.user.currentUser);
  const dispatch = useDispatch();
  const history = useHistory();

  const handleSubmitForm = (values: UserFormValues) => {
    dispatch(updateUser(values));
    history.push(`/${values.username}`);
  };

  return (
    <div className="settings-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Your Settings</h1>
            <UserSettingForm
              currentUser={currentUser}
              onSubmit={handleSubmitForm}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserSetting;
