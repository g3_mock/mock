import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { signInAction } from "../../../redux/actionCreaters/user.action";
import { SignInFormValues } from "../../../models/user.model";
import SignInForm from "../components/SignInForm";
import { RootState } from "../../../models/redux.model";
import { Grid, Paper } from "@material-ui/core";
import styles from "./userSignInOrSignUp.module.css";

//initialValues
//handle submit send to SignIn Form
const UserSignIn = () => {
  const { isAuthenticated, authLoading } = useSelector(
    (state: RootState) => state.user
  );

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (isAuthenticated) {
      history.push("/");
    }
  }, [isAuthenticated, history]);

  const handleSubmit = (values: SignInFormValues) => {
    dispatch(signInAction(values));
  };

  return (
    <div className={styles.background}>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={10}>
          <Paper className={styles.form}>
            <Grid container>
              <Grid item xs={12} sm={6}>
                <div className={styles.image}></div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <div className="pt-5 px-5">
                  <h2 className="font-weight-bold text-center">Sign in</h2>
                  <SignInForm onSubmit={handleSubmit} loading={authLoading} />
                </div>
                <p className="mt-2 text-center font-weight-light text-muted ">
                  Don't have an account?
                  <Link to="/register" className="font-weight-bold">
                    &nbsp;Create your account
                  </Link>
                  .
                </p>
                <p className="mt-3 text-sm-center text-muted font-weight-light">
                  Go to
                  <Link className="font-weight-bold" to="/">
                    &nbsp;home page
                  </Link>
                  .
                </p>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default UserSignIn;
