import { Button, TextField } from "@material-ui/core";
import { useFormik } from "formik";
import React from "react";
import * as yup from "yup";
import { User, UserFormValues } from "../../../models/user.model";
interface Props {
  currentUser: User;
  onSubmit: (values: UserFormValues) => void;
}
const validationSchema = yup.object({
  username: yup
    .string()
    .min(6, "Username should be of minimum 6 characters length")
    .required("Username is required")
    .matches(/^[a-zA-Z0-9_]{6,}$/, "Username must not use special characters"),

  email: yup
    .string()
    .email("Enter a valid email")
    .required("Email is required")
    .matches(
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
      "Invalid Email"
    ),
  password: yup
    .string()
    .min(6, "Password should be of minimum 6 characters length"),
  image: yup.string(),
  bio: yup.string().nullable(),
});
export const UserSettingForm = ({ currentUser, onSubmit }: Props) => {
  const formik = useFormik({
    initialValues: {
      email: currentUser.email,
      username: currentUser.username,
      password: "",
      image: currentUser.image,
      bio: currentUser.bio,
    },
    onSubmit: (values, actions) => {
      actions.setSubmitting(true);
      onSubmit(values);
    },
    validationSchema,
  });

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <fieldset>
          <div>
            <TextField
              fullWidth
              id="image"
              name="image"
              label="Avatar"
              variant="outlined"
              size="medium"
              margin="normal"
              placeholder="URL of profile picture"
              value={formik.values.image}
              onChange={formik.handleChange}
              error={formik.touched.image && Boolean(formik.errors.image)}
              helperText={formik.touched.image && formik.errors.image}
            />
          </div>
          <div>
            <TextField
              fullWidth
              id="username"
              name="username"
              label="Username"
              variant="outlined"
              size="medium"
              placeholder="Your Name"
              margin="normal"
              value={formik.values.username}
              onChange={formik.handleChange}
              error={formik.touched.username && Boolean(formik.errors.username)}
              helperText={formik.touched.username && formik.errors.username}
            />
          </div>
          <div>
            <TextField
              fullWidth
              id="bio"
              name="bio"
              label="Biography"
              variant="outlined"
              size="medium"
              placeholder="Short bio about you"
              multiline
              rows={8}
              margin="normal"
              value={formik.values.bio}
              onChange={formik.handleChange}
              error={formik.touched.bio && Boolean(formik.errors.bio)}
              helperText={formik.touched.bio && formik.errors.bio}
            />
          </div>
          <div>
            <TextField
              fullWidth
              id="email"
              name="email"
              label="Email"
              variant="outlined"
              size="medium"
              margin="normal"
              placeholder="abc@gmail.com"
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
            />
          </div>
          <div>
            <TextField
              fullWidth
              id="password"
              name="password"
              label="Password"
              variant="outlined"
              size="medium"
              margin="normal"
              value={formik.values.password}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
            />
          </div>
          <div className="d-flex justify-content-center">
            <Button
              color="primary"
              size="large"
              className="mt-3 mb-5"
              variant="contained"
              type="submit"
              disabled={formik.isSubmitting && formik.isValid && formik.dirty}
            >
              Update settings
            </Button>
          </div>
        </fieldset>
      </form>
    </div>
  );
};
