import React, { useEffect } from "react";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import { useFormik } from "formik";
import * as yup from "yup";
import { SignInFormValues } from "../../../models/user.model";
import { useSelector } from "react-redux";
import { RootState } from "../../../models/redux.model";

const validationSchema = yup.object({
  email: yup
    .string()
    .email("Enter a valid email")
    .required("Email is required")
    .matches(
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
      "Invalid Email"
    ),
  password: yup
    .string()
    .min(6, "Password should be of minimum 6 characters length")
    .required("Password is required"),
});

interface Props {
  onSubmit: (values: SignInFormValues) => void;
  loading: boolean;
}

const SignInForm = ({ onSubmit, loading }: Props) => {
  const { errorAuth } = useSelector((state: RootState) => state.user);
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, actions) => {
      actions.setSubmitting(true);
      onSubmit(values);
    },
  });

  useEffect(() => {
    if (errorAuth) {
      formik.setSubmitting(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errorAuth]);

  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
        <TextField
          fullWidth
          id="email"
          name="email"
          label="Email"
          size="medium"
          margin="normal"
          value={formik.values.email}
          onChange={formik.handleChange}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
        />
      </div>
      <div>
        <TextField
          fullWidth
          id="password"
          name="password"
          label="Password"
          margin="normal"
          type="password"
          size="medium"
          value={formik.values.password}
          onChange={formik.handleChange}
          error={formik.touched.password && Boolean(formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
        />
      </div>
      <div className="d-flex justify-content-center">
        <Button
          color="primary"
          size="large"
          className="my-3"
          variant="outlined"
          type="submit"
          startIcon={loading ? <CircularProgress size={20} /> : ""}
          disabled={formik.isSubmitting}
        >
          Sign In
        </Button>
      </div>
    </form>
  );
};

export default SignInForm;
