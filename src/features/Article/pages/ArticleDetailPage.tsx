import { Button, CircularProgress } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory, useParams } from "react-router-dom";
import commentIcon from "../../../assets/comment.png";
import loveIcon from "../../../assets/reactions_love.png";
import { RootState } from "../../../models/redux.model";
import {
  getArticleAction,
  likeArticleAction,
  unLikeArticleAction,
} from "../../../redux/actionCreaters/article.action";
import ArticleActionButton from "../components/ArticleActionButton";
import ArticleComment from "../components/ArticleComment";
import ArticleNewModal from "../components/ArticleNewModal";
import styles from "./ArticleDetailPage.module.css";

const ArticleDetailPage = () => {
  const [openModal, setOpenModal] = React.useState(false);
  const {
    article: { article, comments, error, loading },
    user: { currentUser },
  } = useSelector((state: RootState) => state);

  const { slug } = useParams<{ slug: string }>();
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getArticleAction(slug));
  }, [dispatch, slug]);

  useEffect(() => {
    if (error) {
      history.push("/");
    }
  }, [error, history]);

  const handleLikeOrUnLikeClick = () => {
    if (article) {
      if (article.favorited) {
        dispatch(unLikeArticleAction(article.slug));
      } else {
        dispatch(likeArticleAction(article.slug));
      }
    }
  };
  const handleOpenModal = () => {
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  return article && !loading ? (
    <div className="article-page">
      <div className={styles.banner}>
        <div className="container">
          <Button
            style={{ color: "white" }}
            onClick={() => {
              history.goBack();
            }}
            startIcon={<ArrowBack />}
          >
            Back
          </Button>
          <h1>{article.title}</h1>

          <div className="article-meta">
            <Link to={`/${article.author.username}`}>
              <img
                alt=""
                src={article.author.image}
                onError={(e) =>
                  (e.currentTarget.src =
                    "https://static.productionready.io/images/smiley-cyrus.jpg")
                }
              />
            </Link>
            <div className="info">
              <Link
                to={`/${article.author.username}`}
                className="author text-white"
              >
                {article.author.username}
              </Link>
              <span className="date">
                {moment(article.createdAt).format("DD-MM-YYYY hh:mm:ss")}
              </span>
            </div>

            <ArticleActionButton
              article={article}
              onOpenModal={handleOpenModal}
            />
            <ArticleNewModal
              open={openModal}
              onCloseModal={handleCloseModal}
              article={article}
            />
          </div>
        </div>
      </div>

      <div className="container page">
        <div className={`post ${styles.post}`}>
          <div className="post-description">
            <p>{article.body}</p>
          </div>

          <div className="post-state">
            <div
              className={`post-state-btns ${article.favorited && "loved"}`}
              onClick={handleLikeOrUnLikeClick}
            >
              <img src={loveIcon} className={styles.icon} alt="" />
              {article.favoritesCount}
              <span> Love </span>
            </div>
            <div className="post-state-btns">
              <img src={commentIcon} className={styles.icon} alt="" />
              {comments.length}
              <span> Comments</span>
            </div>
          </div>
          <ArticleComment
            currentUser={currentUser}
            article={article}
            comments={comments}
          />
        </div>
      </div>
    </div>
  ) : (
    <div className="mt-5 d-flex justify-content-center">
      <div className="pt-5">
        <CircularProgress size={40} />
      </div>
    </div>
  );
};

export default ArticleDetailPage;
