import React, { useEffect, useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import queryString from "query-string";
import { useDispatch, useSelector } from "react-redux";
import PaginationComponent from "../../../components/Pagination/PaginationComponent";
import { Article } from "../../../models/article.model";
import { RootState } from "../../../models/redux.model";
import {
  getArticlesAction,
  getArticlesFeed,
  getTagsAction
} from "../../../redux/actionCreaters/article.action";
import ArticleList from "../components/ArticleList";
import ArticleLoading from "../components/ArticleLoading";
import ArticleNewButton from "../components/ArticleNewButton";
import ArticleNewModal from "../components/ArticleNewModal";
import ArticleTagList from "../components/ArticleTagList";
import SideBar from "../components/SideBar";
import Story from "../components/Story";
import styles from "./ArticleHomePage.module.css";

const ArticleHomePage = () => {
  const {
    user: { loading: userLoading, currentUser },
    article: { articles, articlesCount, tags, loading },
  } = useSelector((state: RootState) => state);
  const [typeFeed, setTypeFeed] = useState(2); // 1: Your feed && 2: Global feed && 3: Tag feed
  const [tagSelected, setTagSelected] = useState("");
  const [articleList, setArticleList] = useState<Article[]>([]);
  const [pagination, setPagination] = useState({
    page: 1,
    limit: 5,
    totalRows: 0,
  });

  const [filters, setFilters] = useState({
    limit: 5,
    offset: 0,
    tag: "",
  });
  const [openModal, setOpenModal] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    if (!userLoading) {
      const paramsString = queryString.stringify(filters);
      if (typeFeed === 1) {
        //select feed by Feed Articles (display article of flowing user)
        dispatch(getArticlesFeed(paramsString));
      } else {
        dispatch(getArticlesAction(paramsString));
      }
    }
  }, [filters, dispatch, typeFeed, userLoading]);

  useEffect(() => {
    dispatch(getTagsAction());
  }, [dispatch]);

  useEffect(() => {
    setPagination((pagination) => ({
      ...pagination,
      totalRows: articlesCount,
    }));
  }, [articlesCount]);

  useEffect(() => {
    setArticleList(articles);
  }, [articles]);

  const handlePageChange = (newPage: number) => {
    setFilters({
      ...filters,
      offset: (newPage - 1) * filters.limit,
    });
  };

  const handleClickTag = (tag: string) => {
    setTagSelected(tag);
    setArticleList([]);
    setTypeFeed(3);
    setFilters({
      ...filters,
      offset: 0,
      tag: tag,
    });
  };

  const handleClickFeedToggle = (type: number) => {
    setArticleList([]);
    setTypeFeed(type);
    if (type !== 3) {
      setTagSelected("");
      setFilters({
        ...filters,
        offset: 0,
        tag: "",
      });
    }
  };
  const handleOpenModal = () => {
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  return (
    <div className={` ${styles.bodyPage} pt-2 bg-light`}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={3}>
          <SideBar
            onClick={handleClickFeedToggle}
            tagSelected={tagSelected}
            typeFeed={typeFeed}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Grid container>
            <Grid item xs={12}>
              <Story />
            </Grid>

            <Grid item xs={12}>
              <Paper className={styles.formArticle}>
                {currentUser && (
                  <ArticleNewButton
                    currentUser={currentUser}
                    onOpenModal={handleOpenModal}
                  />
                )}
                <ArticleNewModal
                  open={openModal}
                  onCloseModal={handleCloseModal}
                />
              </Paper>
            </Grid>
            <Grid item xs={12}>
              {loading ? (
                <ArticleLoading/>
              ) : (
                <ArticleList articles={articleList} />
              )}
            </Grid>
            <Grid item xs={12}>
              {articleList.length > 0 ? (
                <div className="d-flex justify-content-center">
                  <PaginationComponent
                    pagination={pagination}
                    onPageChange={handlePageChange}
                  />
                </div>
              ) : null}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={3}>
          <ArticleTagList tags={tags} onClick={handleClickTag} />
        </Grid>
      </Grid>
    </div>
  );
};

export default ArticleHomePage;
