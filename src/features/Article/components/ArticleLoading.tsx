import { Skeleton } from "@material-ui/lab";
import React from "react";

const ArticleLoading = () => {
  return (
    <div>
      <div className="my-3 d-flex">
        <Skeleton animation="wave" variant="circle" width={40} height={40} />
        <Skeleton className="ml-2" variant="text" width={200} />
      </div>
      <Skeleton className="w-100" variant="rect" width={210} height={118} />
      <Skeleton className="w-100" variant="text" />
      <Skeleton className="w-100" variant="text" />
    </div>
  );
};

export default ArticleLoading;
