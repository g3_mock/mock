import {
  Button,
  Chip,
  createStyles,
  makeStyles,
  Paper,
  TextField,
  Theme,
} from "@material-ui/core";
import { useFormik } from "formik";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import * as yup from "yup";
import { Article } from "../../../models/article.model";
import {
  createArticle,
  updateArticle,
} from "../../../redux/actionCreaters/article.action";

interface Props {
  currentArticle?: Article;
  onCloseModal: () => void;
}

const validationSchema = yup.object({
  title: yup
    .string()
    .required("Title can't be blank")
    .min(1, "Title is too short (minimum is 1 character)"),
  description: yup
    .string()
    .required("Description can't be blank")
    .min(1, "Description is too short (minimum is 1 character)"),
  body: yup.string().required("Body can't be blank"),
  tag: yup.string().min(1, "to short"),
  tagList: yup.array(),
});

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap",
      listStyle: "none",
      padding: theme.spacing(0.5),
      margin: 0,
    },
    chip: {
      margin: theme.spacing(0.5),
    },
  })
);
const ArticleForm = ({ currentArticle, onCloseModal }: Props) => {
  const dispatch = useDispatch();
  let { slug } = useParams() as any;
  const classes = useStyles();
  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      body: "",
      tagList: [],
      tag: "",
    },
    onSubmit: (values, actions) => {
      actions.setSubmitting(true);
      if (slug) {
        dispatch(
          updateArticle(slug, {
            title: values.title,
            description: values.description,
            body: values.body,
            tagList: values.tagList,
          })
        );
      } else {
        dispatch(
          createArticle({
            title: values.title,
            description: values.description,
            body: values.body,
            tagList: values.tagList,
          })
        );
      }
      onCloseModal();
    },
    validationSchema,
  });
  useEffect(() => {
    if (currentArticle) {
      formik.setValues({
        title: currentArticle.title,
        description: currentArticle.description,
        body: currentArticle.body,
        tagList: currentArticle.tagList as any,
        tag: "",
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentArticle]);

  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
        <TextField
          fullWidth
          id="title"
          name="title"
          label="Article Title"
          variant="outlined"
          size="medium"
          margin="normal"
          placeholder="Article Title"
          value={formik.values.title}
          onChange={formik.handleChange}
          error={formik.touched.title && Boolean(formik.errors.title)}
          helperText={formik.touched.title && formik.errors.title}
        />
      </div>
      <div>
        <TextField
          fullWidth
          id="description"
          name="description"
          label="What's this article about?"
          variant="outlined"
          size="medium"
          margin="normal"
          placeholder="What's this article about?"
          value={formik.values.description}
          onChange={formik.handleChange}
          error={
            formik.touched.description && Boolean(formik.errors.description)
          }
          helperText={formik.touched.description && formik.errors.description}
        />
      </div>
      <div>
        <TextField
          fullWidth
          id="body"
          name="body"
          label="Write your article"
          variant="outlined"
          size="medium"
          margin="normal"
          placeholder="Write your article"
          multiline
          rows={4}
          value={formik.values.body}
          onChange={formik.handleChange}
          error={formik.touched.body && Boolean(formik.errors.body)}
          helperText={formik.touched.body && formik.errors.body}
        />
      </div>
      <div>
        <div>
          <TextField
            fullWidth
            id="tag"
            name="tag"
            label="Tags Enter"
            variant="outlined"
            size="medium"
            margin="normal"
            error={formik.touched.tag && Boolean(formik.errors.tag)}
            helperText={formik.touched.tag && formik.errors.tag}
            value={formik.values.tag}
            onChange={formik.handleChange}
            onKeyDown={(e) => {
              if (e.keyCode === 13) {
                e.preventDefault();
                formik.setFieldTouched("tag", true);
                if (formik.values.tag.length > 0) {
                  formik.setFieldValue("tagList", [
                    ...formik.values.tagList,
                    formik.values.tag,
                  ]);
                  formik.setFieldValue("tag", "");
                } else {
                  formik.setFieldError("tag", "You must write something...");
                }
              }
            }}
          />
          {formik.values.tagList.length > 0 ? (
            <Paper component="ul" className={classes.root}>
              {formik.values.tagList.map((tag, index) => {
                return (
                  <li key={index}>
                    <Chip
                      label={tag}
                      onDelete={() =>
                        formik.setFieldValue(
                          "tagList",
                          formik.values.tagList.filter(
                            (tagDeltete, indexDelete) => indexDelete !== index
                          )
                        )
                      }
                      className={classes.chip}
                    />
                  </li>
                );
              })}
            </Paper>
          ) : null}
        </div>
      </div>

      <div className="d-flex justify-content-center">
        <Button
          color="primary"
          size="large"
          className="mt-3"
          variant="contained"
          type="submit"
          disabled={formik.isSubmitting && formik.isValid && formik.dirty}
        >
          Publish Article
        </Button>
      </div>
    </form>
  );
};

export default ArticleForm;
