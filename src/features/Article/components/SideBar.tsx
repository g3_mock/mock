import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { AssignmentInd, Loyalty, Public } from "@material-ui/icons";
import React, { useEffect } from "react";
import { RootState } from "../../../models/redux.model";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      minWidth: 320,
      backgroundColor: "#F8F9FA",
      paddingLeft: 10,
      paddingRight: 10,
    },
    itemList: {
      borderRadius: 5,
    },
  })
);

interface Props {
  onClick: (type: number) => void;
  typeFeed: number;
  tagSelected: string;
}

export default function SideBar(props: Props) {
  const classes = useStyles();
  const [selectedIndex, setSelectedIndex] = React.useState<number>();
  const { isAuthenticated } = useSelector((state: RootState) => state.user);
  useEffect(() => {
    if (props.typeFeed) {
      setSelectedIndex(props.typeFeed);
    }
  }, [props]);
  const handleListItemClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    index: number
  ) => {
    props.onClick(index);
  };

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem
          button
          className={classes.itemList}
          selected={selectedIndex === 2}
          onClick={(event) => handleListItemClick(event, 2)}
        >
          <ListItemIcon>
            <Public />
          </ListItemIcon>
          <ListItemText primary="Global Feeds" />
        </ListItem>
        {isAuthenticated && (
          <ListItem
            button
            className={classes.itemList}
            selected={selectedIndex === 1}
            onClick={(event) => handleListItemClick(event, 1)}
          >
            <ListItemIcon>
              <AssignmentInd />
            </ListItemIcon>
            <ListItemText primary="Your Feeds" />
          </ListItem>
        )}

        {props.tagSelected && (
          <ListItem
            button
            className={classes.itemList}
            selected={selectedIndex === 3}
            onClick={(event) => handleListItemClick(event, 3)}
          >
            <ListItemIcon>
              <Loyalty />
            </ListItemIcon>
            <ListItemText primary={`#${props.tagSelected}`} />
          </ListItem>
        )}
      </List>
    </div>
  );
}
