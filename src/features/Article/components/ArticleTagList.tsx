import { Chip, createStyles, makeStyles, Theme } from "@material-ui/core";
import { Loyalty } from "@material-ui/icons";
import { Skeleton } from "@material-ui/lab";
import React from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap",
      "& > *": {
        margin: theme.spacing(0.5),
      },
    },
  })
);

interface Props {
  tags: string[];
  onClick: (tag: string) => void;
}

const ArticleTagList = ({ tags, onClick }: Props) => {
  const classes = useStyles();
  return (
    <div>
      <div className="sidebar">
        <p className="text-center">Popular Tags</p>
        <div className={classes.root}>
          {tags.length > 0 ? (
            tags.map(
              (tag, i) =>
                //Remove all special characters except space from a tag
                tag.replace(/[^a-zA-Z ]/g, "") && (
                  <Chip
                    key={i}
                    icon={<Loyalty />}
                    label={tag}
                    onClick={(e) => onClick(tag)}
                    // color="primary"
                  />
                )
            )
          ) : (
            <>
              <Skeleton variant="text" className="w-100" />
              <Skeleton variant="text" className="w-100" />
              <Skeleton variant="text" className="w-100" />
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default ArticleTagList;
