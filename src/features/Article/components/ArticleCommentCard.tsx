import React from "react";
import { Link } from "react-router-dom";
import { Comment } from "../../../models/article.model";
import { Delete } from "@material-ui/icons";
import { IconButton } from "@material-ui/core";
import moment from "moment";
interface Props {
  comment: Comment;
  ableToDelete: boolean;
  onDeleteComment: () => void;
}

const ArticleCommentCard = ({
  comment,
  ableToDelete,
  onDeleteComment,
}: Props) => {
  return (
    <div className="post-comments-single">
      <div className="post-comment-avatar">
        <Link to={`/${comment.author.username}`}>
          <img
            src={comment.author.image}
            alt=""
            onError={(e) =>
              (e.currentTarget.src =
                "https://static.productionready.io/images/smiley-cyrus.jpg")
            }
          />
        </Link>
      </div>
      <div className="post-comment-text">
        <div className="post-comment-text-inner">
          <h6>
            <Link to={`/${comment.author.username}`}>
              {comment.author.username}
            </Link>
          </h6>
          <p> {comment.body} </p>
        </div>
        <div className="uk-text-small">
          <span> {moment(comment.createdAt).startOf("seconds").fromNow()}</span>
        </div>
      </div>
      {ableToDelete && (
        <span>
          <IconButton aria-label="delete" onClick={onDeleteComment}>
            <Delete />
          </IconButton>
        </span>
      )}
    </div>
  );
};

export default ArticleCommentCard;
