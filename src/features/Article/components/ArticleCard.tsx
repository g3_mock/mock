import {
  Box,
  createStyles,
  Grid,
  IconButton,
  ListItemText,
  makeStyles,
  Menu,
  MenuItem,
  MenuProps,
  Theme,
  Typography,
  withStyles,
} from "@material-ui/core";
import {
  CancelPresentation,
  Delete,
  MoreHoriz,
  Schedule,
} from "@material-ui/icons";
import moment from "moment";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import loveIcon from "../../../assets/reactions_love.png";
import { Article } from "../../../models/article.model";
import { RootState } from "../../../models/redux.model";
import {
  deleteArticleAction,
  hiddenArticlePost,
  likeArticleAction,
  unLikeArticleAction,
} from "../../../redux/actionCreaters/article.action";
import styles from "./ArticleCard.module.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    bgGrey: {
      backgroundColor: "#F8F9FA",
      minHeight: 50,
    },
    content: {
      padding: 20,
      marginTop: 7,
      marginBottom: 7,
      overflow: "hidden",
    },
  })
);

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
    paddingLeft: 10,
    paddingRight: 10,
    width: 250,
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

interface Props {
  article: Article;
}

const ArticleCard = ({ article }: Props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { currentUser, isAuthenticated } = useSelector(
    (state: RootState) => state.user
  );

  const history = useHistory();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLikeOrUnLikeClick = () => {
    if (!currentUser) {
      history.push("/login");
    }
    if (article.favorited) {
      dispatch(unLikeArticleAction(article.slug));
    } else {
      dispatch(likeArticleAction(article.slug));
    }
  };

  return (
    <div className={`${styles.post} post mt-3 mb-2`}>
      <div className="post-heading">
        <div className="post-avature">
          <Link to={`/${article.author.username}`}>
            <img
              src={article.author.image}
              alt=""
              onError={(e) =>
                (e.currentTarget.src =
                  "https://static.productionready.io/images/smiley-cyrus.jpg")
              }
            />
          </Link>
        </div>
        {/* User Information */}
        <Grid container>
          <Grid item className="post-title">
            <h4>
              <Link to={`/${article.author.username}`}>
                {article.author.username}
              </Link>{" "}
            </h4>
            <p>
              <Schedule fontSize="small" />
              <span>
                {" " + moment(article.updatedAt).startOf("seconds").fromNow()}
              </span>
            </p>
          </Grid>
          <Grid item>
            <IconButton onClick={handleClick}>
              <MoreHoriz />
            </IconButton>
            <StyledMenu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem
                onClick={() => {
                  handleClose();
                  dispatch(hiddenArticlePost(article.slug));
                }}
              >
                <CancelPresentation />
                <ListItemText
                  style={{ marginLeft: 10 }}
                  primary={"Hide Post"}
                  secondary="See fewer post like this"
                />
              </MenuItem>
              {isAuthenticated &&
                currentUser.username === article.author.username && (
                  <MenuItem
                    onClick={() => {
                      handleClose();
                      dispatch(deleteArticleAction(article.slug));
                    }}
                  >
                    <Delete />
                    <ListItemText
                      style={{ marginLeft: 10 }}
                      primary={"Delete"}
                      secondary="Delete Your Article"
                    />
                  </MenuItem>
                )}
            </StyledMenu>
          </Grid>
        </Grid>
      </div>
      <div className="post-description">
        <div>
          <Box className={`${classes.bgGrey}`}>
            <Typography
              className={styles.textTitle}
              style={{ fontFamily: "Lora" }}
              variant="h5"
              component="h3"
              align="center"
            >
              {article.title}
            </Typography>
            <Typography
              variant="body1"
              component="pre"
              color="textSecondary"
              className={classes.content}
            >
              {article.description}
            </Typography>
          </Box>

          <ul className="tag-list mb-0 float-right">
            {article.tagList.map(
              (tag, i) =>
                tag.replace(/[^a-zA-Z ]/g, "") && (
                  <li
                    key={i}
                    className="tag-default tag-pill tag-outline ng-binding ng-scope"
                  >
                    {tag}
                  </li>
                )
            )}
          </ul>
          <div className="clearfix"></div>
        </div>
        <div className="post-state-details">
          <div>
            <img src={loveIcon} alt="" />
            <p> {article.favoritesCount} </p>
          </div>
        </div>
      </div>

      <div className="post-state">
        <div
          className={`post-state-btns ${article.favorited && "loved"}`}
          onClick={handleLikeOrUnLikeClick}
        >
          <img src={loveIcon} className={styles.icon} alt="" />
          {article.favoritesCount}
          <span> Love </span>
        </div>
        <div className="post-state-btns">
          <Link to={`/article/${article.slug}`} className="preview-link">
            <span>Read more...</span>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ArticleCard;
