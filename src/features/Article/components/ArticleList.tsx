import React from "react";
import { Article } from "../../../models/article.model";
import ArticleCard from "./ArticleCard";
interface Props {
  articles: Article[];
}

const ArticleList = ({ articles }: Props) => {
  return (
    <div>
      {articles.map((article) => (
        <ArticleCard key={article.slug} article={article} />
      ))}
    </div>
  );
};

export default ArticleList;
