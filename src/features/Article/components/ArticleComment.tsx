import React from "react";
import { useDispatch } from "react-redux";
import { Article, Comment } from "../../../models/article.model";
import { User } from "../../../models/user.model";
import { deleteCommentAction } from "../../../redux/actionCreaters/article.action";
import ArticleCommentCard from "./ArticleCommentCard";
import ArticleCommentForm from "./ArticleCommentForm";
import Swal from "sweetalert2";

interface Props {
  comments: Comment[];
  article: Article;
  currentUser: User;
}

const ArticleComment = ({ currentUser, comments, article }: Props) => {
  const dispatch = useDispatch();
  const handleDelete = (id: number) => {
    Swal.fire({
      title: "Are you sure to remove this comment?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "OK",
    }).then((result) => {
      if (result.value) {
        dispatch(deleteCommentAction(article.slug, id));
      }
    });
  };

  return (
    <div className="post-comments">
      <ArticleCommentForm articleSlug={article.slug} />
      {/* get list comments and view list by each item */}
      <div className="mt-4"></div>
      {comments.map((comment) => (
        <ArticleCommentCard
          ableToDelete={currentUser.username === comment.author.username}
          key={comment.id}
          comment={comment}
          onDeleteComment={() => handleDelete(comment.id)}
        />
      ))}
    </div>
  );
};

export default ArticleComment;
