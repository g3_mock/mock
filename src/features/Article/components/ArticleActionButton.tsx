import React, { Fragment } from "react";
import { Button } from "@material-ui/core";
import { Add, Create, Delete } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { Article } from "../../../models/article.model";
import {
  deleteArticleAction,
  followUserArticle,
  unfollowUserArticle,
} from "../../../redux/actionCreaters/article.action";

interface Props {
  article: Article;
  onOpenModal: () => void;
}

const ArticleActionButton = ({ article, onOpenModal }: Props) => {
  const currentUser = useSelector((state: any) => state.user.currentUser);
  const dispatch = useDispatch();
  const history = useHistory();

  const handleDeleteArticle = () => {
    if (article) {
      Swal.fire({
        title: "Are you sure to remove this article?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
      }).then((result) => {
        if (result.value) {
          dispatch(deleteArticleAction(article.slug));
          history.push("/");
        }
      });
    }
  };

  const handleEditArticle = () => {
    // history.push(`/editor/${article.slug}`);
    onOpenModal();
  };

  const handleFollowOrUnfollow = (username: string) => {
    if (article.author.following) {
      dispatch(unfollowUserArticle(username));
    } else {
      dispatch(followUserArticle(username));
    }
  };

  return currentUser ? (
    currentUser.username === article.author.username ? (
      <Fragment>
        <Button
          className="mr-2"
          variant="contained"
          color="primary"
          startIcon={<Create />}
          onClick={handleEditArticle}
        >
          Edit Article
        </Button>
        <Button
          variant="contained"
          color="secondary"
          startIcon={<Delete />}
          onClick={handleDeleteArticle}
        >
          Delete Article
        </Button>
      </Fragment>
    ) : (
      <Fragment>
        <Button
          className="mr-2"
          variant="contained"
          color="primary"
          startIcon={<Add />}
          onClick={() => {
            handleFollowOrUnfollow(article.author.username);
          }}
        >
          {article.author.following ? "Unfollow" : "Follow"}{" "}
          {article.author.username}
        </Button>
      </Fragment>
    )
  ) : null;
};

export default ArticleActionButton;
