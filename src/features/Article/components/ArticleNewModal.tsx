import {
  createStyles,
  IconButton,
  makeStyles,
  Modal,
  Theme,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import React from "react";
import ArticleForm from "./ArticleForm";

interface Props {
  onCloseModal: () => void;
  open: boolean;
  article?: any;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      width: 600,
      height: 600,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 5, 3),
    },
    paper: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  })
);

export default function ArticleNewModal({
  onCloseModal,
  open,
  article,
}: Props) {
  const classes = useStyles();
  return (
    <div>
      <Modal
        className={classes.paper}
        open={open}
        onClose={() => onCloseModal()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <>
          <div className={classes.modal}>
            <div className="d-flex flex-row-reverse">
              <IconButton aria-label="delete" onClick={() => onCloseModal()}>
                <CloseIcon />
              </IconButton>
            </div>
            {article ? (
              <ArticleForm
                onCloseModal={onCloseModal}
                currentArticle={article}
              />
            ) : (
              <ArticleForm onCloseModal={onCloseModal} />
            )}
          </div>
        </>
      </Modal>
    </div>
  );
}
