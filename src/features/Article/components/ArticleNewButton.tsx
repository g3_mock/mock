import {
  Avatar,
  createStyles,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Theme,
} from "@material-ui/core";
import React from "react";
import AssignmentIcon from "@material-ui/icons/Assignment";
import LoyaltyIcon from "@material-ui/icons/Loyalty";
import styles from "./ArticleNewButton.module.css";

interface Props {
  currentUser: any;
  onOpenModal: () => void;
}
const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      width: "100%",
      backgroundColor: theme.palette.background.paper,
    },
  });
});

export default function ArticleNewButton({ currentUser, onOpenModal }: Props) {
  const classes = useStyles();

  return (
    <div className={`${classes.root} ${styles.formArticle} `}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem button onClick={() => onOpenModal()}>
          <ListItemIcon>
            <Avatar alt="" src={currentUser.image} />
          </ListItemIcon>
          <ListItemText
            className={`${styles.newarticle} text-secondary`}
            primary={`What's your mind? ${currentUser.username}`}
          />
        </ListItem>
      </List>
      <Divider />
      <List
        component="nav"
        aria-label="secondary mailbox folders"
        className="d-flex"
      >
        <ListItem
          button
          onClick={() => onOpenModal()}
          className="d-flex justify-content-around"
        >
          <div className="d-flex">
            <ListItemIcon>
              <Avatar variant="rounded" className={styles.icon}>
                <AssignmentIcon fontSize="small" />
              </Avatar>
            </ListItemIcon>
            <ListItemText className="text-secondary" primary="New Article" />
          </div>
        </ListItem>
        <ListItem
          button
          onClick={() => onOpenModal()}
          className="d-flex justify-content-around"
        >
          <div className="d-flex">
            <ListItemIcon>
              <Avatar variant="rounded" className={styles.icon}>
                <LoyaltyIcon fontSize="small" />
              </Avatar>
            </ListItemIcon>
            <ListItemText primary="New tag" className="text-secondary" />
          </div>
        </ListItem>
      </List>
    </div>
  );
}
