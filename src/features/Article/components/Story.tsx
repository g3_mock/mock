import React from "react";
import viet from "../../../assets/viet.jpg";
import hien from "../../../assets/hien.jpg";
import duc from "../../../assets/duc.jpg";
import styles from "./Story.module.css";

const Story = () => {
  return (
    <div className={styles.story}>
      <ul className="d-flex">
        <li className="col-4">
          <div>
            <div
              className={`${styles.image1} story-card h-100`}
              data-src="assets/images/avatars/avatar-lg-1.jpg"
            >
              <img src={duc} alt="" />
              <div className="story-card-content">
                <h4> Trung Duc </h4>
              </div>
            </div>
          </div>
        </li>
        <li className="col-4">
          <div>
            <div className={`${styles.image2} story-card h-100`}>
              <img src={hien} alt="" />
              <div className="story-card-content">
                <h4> Thanh Hien </h4>
              </div>
            </div>
          </div>
        </li>
        <li className="col-4">
          <div>
            <div className={`${styles.image3} story-card h-100`}>
              <img src={viet} alt="" />
              <div className="story-card-content">
                <h4> Tran Viet </h4>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default Story;
