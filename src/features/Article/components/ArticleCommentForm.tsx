import React from "react";
import { IconButton } from "@material-ui/core";
import { Send } from "@material-ui/icons";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";
import { addCommentAction } from "../../../redux/actionCreaters/article.action";

interface Props {
  articleSlug: string;
}
const validationSchema = Yup.object({
  body: Yup.string().min(1, "You must comment something!").required(),
});

const ArticleCommentForm = ({ articleSlug }: Props) => {
  const currentUser = useSelector((state: any) => state.user.currentUser);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      body: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, actions) => {
      dispatch(
        addCommentAction(articleSlug, { comment: { body: values.body } })
      );
      actions.setValues({ body: "" });
      actions.setSubmitting(false);
    },
  });

  return currentUser ? (
    <form onSubmit={formik.handleSubmit}>
      <div className="post-comments p-0">
        <div className="post-add-comment">
          <div className="post-comment-avatar">
            <img
              src={currentUser.image}
              alt=""
              onError={(e) =>
                (e.currentTarget.src =
                  "https://static.productionready.io/images/smiley-cyrus.jpg")
              }
            />
          </div>
          <div className="post-add-comment-text-area">
            <input
              className="pt-2"
              id="outlined-multiline-static"
              name="body"
              placeholder="Write a comment..."
              value={formik.values.body}
              onChange={formik.handleChange}
            />
          </div>
          <IconButton color="primary" type="submit">
            <Send />
          </IconButton>
        </div>
      </div>
    </form>
  ) : null;
};

export default ArticleCommentForm;
