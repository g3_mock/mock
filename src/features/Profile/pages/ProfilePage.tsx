import { CircularProgress } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  NavLink,
  Route,
  Switch,
  useHistory,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import { RootState } from "../../../models/redux.model";
import { getProfileAction } from "../../../redux/actionCreaters/profile.action";
import ProfileInfor from "../components/ProfileInfor";
import ProfileFavorites from "./ProfileFavorites";
import ProfileOwn from "./ProfileOwn";
import styles from "./ProfilePage.module.css";

const ProfilePage = () => {
  const match = useRouteMatch();
  const { username } = useParams<{ username: string }>();
  const {
    profile: { error, profile },
  } = useSelector((state: RootState) => state);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProfileAction(username));
  }, [username, dispatch]);

  useEffect(() => {
    if (error) {
      history.push("/");
    }
  }, [error, history]);

  return profile ? (
    <div className="profile-page">
      <ProfileInfor profile={profile} />

      <div className="nav-profile">
        <div className="w-100">
          <div className="responsive-tab">
            <ul className={styles.nav_profile}>
              <li>
                <NavLink
                  className={styles.link}
                  activeClassName={styles.active}
                  to={`/${username}`}
                  exact
                >
                  My articles
                </NavLink>
              </li>
              <li>
                <NavLink
                  className={styles.link}
                  activeClassName={styles.active}
                  to={`${match.url}/favorites`}
                  exact
                >
                  Favorited Articles
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="container pb-5">
        <div className="row">
          <div className="col-xs-12 col-md-10 offset-md-1">
            <Switch>
              <Route path={`${match.url}/favorites`}>
                <ProfileFavorites username={username} />
              </Route>
              <Route exact path={`/${username}`}>
                <ProfileOwn username={username} />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div className="mt-5 d-flex justify-content-center">
      <div className="pt-5">
        <CircularProgress size={40} />
      </div>
    </div>
  );
};

export default ProfilePage;
