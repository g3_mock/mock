import React, { Fragment, useEffect, useState } from "react";
import queryString from "query-string";
import { useDispatch, useSelector } from "react-redux";
import PaginationComponent from "../../../components/Pagination/PaginationComponent";
import { RootState } from "../../../models/redux.model";
import { getArticlesAction } from "../../../redux/actionCreaters/article.action";
import ArticleList from "../../Article/components/ArticleList";
import ArticleLoading from "../../Article/components/ArticleLoading";

interface Props {
  username: string;
}

const ProfileFavorites = ({ username }: Props) => {
  const { loading, articles, articlesCount } = useSelector(
    (state: RootState) => state.article
  );
  const [pagination, setPagination] = useState({
    page: 1,
    limit: 5,
    totalRows: 0,
  });

  const [filters, setFilters] = useState({
    limit: 5,
    offset: 0,
    favorited: username,
  });

  const dispatch = useDispatch();

  useEffect(() => {
    const paramsString = queryString.stringify(filters);
    dispatch(getArticlesAction(paramsString));
  }, [filters, dispatch]);

  useEffect(() => {
    setPagination((pagination) => ({
      ...pagination,
      totalRows: articlesCount,
    }));
  }, [articlesCount]);

  const handlePageChange = (newPage: number) => {
    setFilters({
      ...filters,
      offset: (newPage - 1) * filters.limit,
    });
  };
  return (
    <Fragment>
      {!loading && articles.length <= 0 ? (
        <p className="m-3">No articles are here... yet.</p>
      ) : null}

      {loading ? <ArticleLoading /> : <ArticleList articles={articles} />}

      {articles.length > 0 ? (
        <div className="d-flex justify-content-center">
          <PaginationComponent
            pagination={pagination}
            onPageChange={handlePageChange}
          />
        </div>
      ) : null}
    </Fragment>
  );
};

export default ProfileFavorites;
