import { Button } from "@material-ui/core";
import { Add, Settings } from "@material-ui/icons";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import cover from "../../../assets/profile-cover.jpg";
import { Profile } from "../../../models/profile.model";
import { RootState } from "../../../models/redux.model";
import {
  followUser,
  unfollowUser,
} from "../../../redux/actionCreaters/profile.action";
import styles from "./ProfileInfor.module.css";

interface Props {
  profile: Profile;
}

const ProfileInfor = ({ profile }: Props) => {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((state: RootState) => state.user);
  const history = useHistory();
  const handleFollowOrUnfollow = (username: string) => {
    if (!currentUser) {
      history.push("/login");
    }
    if (profile.following) {
      dispatch(unfollowUser(username));
    } else {
      dispatch(followUser(username));
    }
  };

  const handleClickSetting = () => {
    history.push("/settings");
  };

  return (
    <div className="profile">
      <div className="profile-cover">
        <img src={cover} alt="" />
        {currentUser && currentUser.username === profile.username ? (
          <Button
            className={styles.button_action}
            variant="contained"
            color="primary"
            onClick={handleClickSetting}
            startIcon={<Settings />}
          >
            Edit Profile Settings
          </Button>
        ) : (
          <Button
            className={styles.button_action}
            variant="contained"
            color="primary"
            startIcon={<Add />}
            onClick={() => {
              handleFollowOrUnfollow(profile.username);
            }}
          >
            {profile.following ? "Unfollow" : "Follow"} {profile.username}
          </Button>
        )}
      </div>
      <div className="profile-details">
        <div className="profile-image">
          <img
            src={profile.image}
            alt=""
            onError={(e) =>
              (e.currentTarget.src =
                "https://static.productionready.io/images/smiley-cyrus.jpg")
            }
          />
        </div>
        <div className="profile-details-info">
          <h1>{profile.username}</h1>
          <p>{profile.bio}</p>
        </div>
      </div>
    </div>
  );
};

export default ProfileInfor;
