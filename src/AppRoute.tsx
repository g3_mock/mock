import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch } from "react-router-dom";
import NotFound from "./components/layouts/NotFound";
import PrivateRoute from "./components/routing/PrivateRoute";
import ArticleDetailPage from "./features/Article/pages/ArticleDetailPage";
import ArticleHomePage from "./features/Article/pages/ArticleHomePage";
import ProfilePage from "./features/Profile/pages/ProfilePage";
import UserSetting from "./features/User/pages/UserSetting";
import UserSignIn from "./features/User/pages/UserSignIn";
import UserSignUp from "./features/User/pages/UserSignUp";
import { undoNotifyDeletedArticle } from "./helpers/alertNotify";
import { RootState } from "./models/redux.model";

const App = () => {
  const { openUndo } = useSelector((state: RootState) => state.article);
  const dispatch = useDispatch();
  useEffect(() => {
    if (openUndo) {
      undoNotifyDeletedArticle(dispatch);
    }
  }, [openUndo, dispatch]);
  return (
    <Switch>
      <Route exact path="/" component={ArticleHomePage} />
      <PrivateRoute exact path="/settings" component={UserSetting} />
      <PrivateRoute path="/article/:slug" component={ArticleDetailPage} />
      <Route exact path="/login" component={UserSignIn} />
      <Route exact path="/register" component={UserSignUp} />
      <Route path="/:username" component={ProfilePage} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default App;
