import { axiosService } from "./api";

export const getUserProfile = (username: string) => {
  return axiosService.get(`/api/profiles/${username}`);
};

export const followUser = (username: string) => {
  return axiosService.post(`/api/profiles/${username}/follow`);
};

export const unFollowUser = (username: string) => {
  return axiosService.delete(`/api/profiles/${username}/follow`);
};
