import axios from "axios";

export const axiosService = axios.create({
  baseURL: `https://conduit.productionready.io/`,
});

export const setAuthToken = (token: any) => {
  if (token) {
    axiosService.defaults.headers["authorization"] = "Token " + token;
    localStorage.setItem("token", token);
  } else {
    delete axiosService.defaults.headers["authorization"];
    localStorage.removeItem("token");
  }
};
