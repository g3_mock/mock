import { ArticleFormValues, CommentAPIRequest } from "../models/article.model";
import { axiosService } from "./api";

// ============REGION: Articles api services!
export const getFavoriteArticles = (author: string) => {
  return axiosService.get(`/api/articles?favorited=${author}`);
};

export const getArticlesFilter = (paramsString: string) => {
  return axiosService.get(`/api/articles?${paramsString}`);
};

export const getArticlesFeedFilter = (paramsString: string) => {
  return axiosService.get(`/api/articles/feed?${paramsString}`);
};

export const getTags = () => {
  return axiosService.get("/api/tags");
};

export const getArticle = (slug: string) => {
  return axiosService.get(`/api/articles/${slug}`);
};

export const deleteArticle = (slug: string) => {
  return axiosService.delete(`/api/articles/${slug}`);
};

// TODO: Favorite/UnFavorite Article

export const likeArticle = (slug: string) => {
  return axiosService.post(`/api/articles/${slug}/favorite`);
};

export const unLikeArticle = (slug: string) => {
  return axiosService.delete(`/api/articles/${slug}/favorite`);
};

// ============REGION: Comment api services!

export const getComments = (slug: string) => {
  return axiosService.get(`/api/articles/${slug}/comments`);
};

export const addComment = (slug: string, data: CommentAPIRequest) => {
  return axiosService.post(`/api/articles/${slug}/comments`, data);
};

export const deleteComment = (slug: string, id: number) => {
  return axiosService.delete(`/api/articles/${slug}/comments/${id}`);
};

//Create & update article
export const createArticle = (data: ArticleFormValues) => {
  return axiosService.post("/api/articles", { article: data });
};

export const updateArticle = (slug: string, data: ArticleFormValues) => {
  return axiosService.put(`/api/articles/${slug}`, { article: data });
};
