import { SignInFormValues, SignUpFormValues } from "../models/user.model";
import { UserFormValues } from "../models/user.model";
import { axiosService } from "./api";

export const signUp = (data: SignUpFormValues) => {
  return axiosService.post("/api/users ", data);
};

export const signIn = (data: SignInFormValues) => {
  return axiosService.post("/api/users/login ", data);
};

export const getCurrentUser = () => {
  return axiosService.get("/api/user");
};

export const updateUser = (data: UserFormValues) => {
  return axiosService.put("/api/user", data);
};
