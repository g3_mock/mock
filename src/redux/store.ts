import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { setAuthToken } from "../apis/api";
import rootReducer from "./reducers";
import rootSaga from "./sagas/index";

const initialState = {};

const middleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(middleware)
);

middleware.run(rootSaga);

// initialize current state from redux store for subscription comparison
// preventing undefined error
let currentState = store.getState();

store.subscribe(() => {
  // keep track of the previous and current state to compare changes
  let previousState = currentState;
  currentState = store.getState();
  // if the token changes set the value in localStorage and axios headers
  if (previousState.user.token !== currentState.user.token) {
    const token = currentState.user.token;
    setAuthToken(token);
  }
});

export default store;
