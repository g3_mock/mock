import { fork } from "redux-saga/effects";
import {
  addCommentSaga,
  createArticleSaga,
  deleteArticleSaga,
  deleteCommentSaga,
  followUserArticleSaga,
  getArticleSaga,
  getArticlesFeedSaga,
  getArticlesSaga,
  getTagsSaga,
  likeArticleSaga,
  unfollowUserArticleSaga,
  unLikeArticleSaga,
  updateArticleSaga,
} from "./article.saga";
import {
  followUserSaga,
  getProfileSaga,
  unfollowUserSaga,
} from "./profile.saga";
import {
  getCurrentUserSaga,
  signInSaga,
  signUpSaga,
  updateUserSaga,
} from "./user.saga";

function* rootSaga() {
  yield fork(signUpSaga);
  yield fork(signInSaga);
  yield fork(getCurrentUserSaga);
  yield fork(updateUserSaga);

  yield fork(getArticlesSaga);
  yield fork(getArticlesFeedSaga);
  yield fork(getArticleSaga);
  yield fork(deleteArticleSaga);
  yield fork(createArticleSaga);
  yield fork(updateArticleSaga);
  yield fork(getTagsSaga);
  yield fork(addCommentSaga);
  yield fork(deleteCommentSaga);
  yield fork(getProfileSaga);
  yield fork(followUserSaga);
  yield fork(unfollowUserSaga);
  yield fork(followUserArticleSaga);
  yield fork(unfollowUserArticleSaga);

  yield fork(likeArticleSaga);
  yield fork(unLikeArticleSaga);
}

export default rootSaga;
