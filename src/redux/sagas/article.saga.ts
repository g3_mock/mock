import { call, delay, put, race, take } from "redux-saga/effects";
import {
  addComment,
  createArticle,
  deleteArticle,
  deleteComment,
  getArticle,
  getArticlesFeedFilter,
  getArticlesFilter,
  getComments,
  getTags,
  likeArticle,
  unLikeArticle,
  updateArticle,
} from "../../apis/article.service";
import { followUser, unFollowUser } from "../../apis/profile.service";
import { showError } from "../../helpers/alertNotify";
import {
  addCommentSuccess,
  createArticleSuccess,
  deleteArticleSuccess,
  deleteCommentSuccess,
  followUserArticleSuccess,
  getArticleFailed,
  getArticlesFailed,
  getArticlesFeedSuccess,
  getArticlesSuccess,
  getArticleSuccess,
  getCommentsFail,
  getCommentsSuccess,
  getTagsSuccess,
  likeArticleSuccess,
  unfollowUserArticleSuccess,
  unLikeArticleSuccess,
  updateArticleSuccess,
} from "../actionCreaters/article.action";
import { ArticleActionTypes } from "../actionTypes/types";

export function* getArticlesSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.GET_ARTICLES);
    try {
      const resp = yield call(getArticlesFilter, payload);
      const { data } = resp;
      yield put(getArticlesSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
      yield put(getArticlesFailed());
    }
  }
}

export function* getTagsSaga() {
  while (true) {
    yield take(ArticleActionTypes.GET_TAGS);
    try {
      const resp = yield call(getTags);
      const { data } = resp;
      yield put(getTagsSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}
// REGION: manage article
export function* getArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.GET_ARTICLE);
    try {
      const { data } = yield call(getArticle, payload);
      const { data: commentsData } = yield call(getComments, payload);
      yield put(getArticleSuccess(data));
      yield put(getCommentsSuccess(commentsData));
    } catch (error) {
      showError(error.response.data.errors);
      yield put(getArticleFailed());
      yield put(getCommentsFail());
    }
  }
}

export function* getArticlesFeedSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.GET_ARTICLE_FEED);
    try {
      const resp = yield call(getArticlesFeedFilter, payload);
      const { data } = resp;
      yield put(getArticlesFeedSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* deleteArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.DELETE_ARTICLE);
    try {
      const { delayNotify } = yield race({
        delayNotify: delay(3000),
        undo: take(ArticleActionTypes.UNDO_ARTICLE),
      });
      if (delayNotify) {
        yield put(deleteArticleSuccess(payload));
        yield call(deleteArticle, payload);
      }
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* likeArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.LIKE_ARTICLE);
    try {
      const { data } = yield call(likeArticle, payload);
      yield put(likeArticleSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* unLikeArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.UNLIKE_ARTICLE);
    try {
      const { data } = yield call(unLikeArticle, payload);
      yield put(unLikeArticleSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* followUserArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.FOLLOW_USER_ARTICLE);
    try {
      const { data } = yield call(followUser, payload);
      yield put(followUserArticleSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* unfollowUserArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.UNFOLLOW_USER_ARTICLE);
    try {
      const { data } = yield call(unFollowUser, payload);
      yield put(unfollowUserArticleSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* createArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.CREATE_ARTICLE);
    try {
      const { data } = yield call(createArticle, payload);
      yield put(createArticleSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* updateArticleSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.UPDATE_ARTICLE);
    const { slug, data } = payload;
    try {
      const { data: dataResponse } = yield call(updateArticle, slug, data);
      yield put(updateArticleSuccess(dataResponse));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

// REGION: Manage comments:
export function* addCommentSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.ADD_COMMENT);
    const { slug, data } = payload;
    try {
      const response = yield call(addComment, slug, data);
      yield put(addCommentSuccess(response.data.comment));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* deleteCommentSaga() {
  while (true) {
    const { payload } = yield take(ArticleActionTypes.DELETE_COMMENT);
    const { slug, id } = payload;
    try {
      yield put(deleteCommentSuccess(id));
      yield call(deleteComment, slug, id);
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}
