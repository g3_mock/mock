import { call, put, take } from "redux-saga/effects";
import {
  followUser,
  getUserProfile,
  unFollowUser,
} from "../../apis/profile.service";
import { showError } from "../../helpers/alertNotify";
import {
  followUserSuccess,
  getProfileFailed,
  getProfileSuccess,
  unfollowUserSuccess,
} from "../actionCreaters/profile.action";
import { ProfileActionTypes } from "../actionTypes/types";

export function* getProfileSaga() {
  while (true) {
    const { payload } = yield take(ProfileActionTypes.GET_PROFILE);
    try {
      const resp = yield call(getUserProfile, payload);
      const { data } = resp;
      yield put(getProfileSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
      yield put(getProfileFailed());
    }
  }
}

export function* followUserSaga() {
  while (true) {
    const { payload } = yield take(ProfileActionTypes.FOLLOW_USER);
    try {
      const { data } = yield call(followUser, payload);
      yield put(followUserSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}

export function* unfollowUserSaga() {
  while (true) {
    const { payload } = yield take(ProfileActionTypes.UNFOLLOW_USER);
    try {
      const { data } = yield call(unFollowUser, payload);
      yield put(unfollowUserSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}
