import { call, put, take } from "redux-saga/effects";
import {
  getCurrentUser,
  signIn,
  signUp,
  updateUser,
} from "../../apis/user.service";
import { showError } from "../../helpers/alertNotify";
import {
  loadUserFailed,
  loadUserSuccess,
  signInFailed,
  signInSuccess,
  signUpFailed,
  signUpSuccess,
  updateUserSuccess,
} from "../actionCreaters/user.action";
import { UserActionTypes } from "../actionTypes/types";

export function* signUpSaga() {
  while (true) {
    const { payload } = yield take(UserActionTypes.SIGNUP);
    try {
      const resp = yield call(signUp, payload);
      const { data } = resp;
      yield put(signUpSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
      yield put(signUpFailed());
    }
  }
}

export function* signInSaga() {
  while (true) {
    const { payload } = yield take(UserActionTypes.SIGNIN);
    try {
      const resp = yield call(signIn, payload);
      const { data } = resp;
      yield put(signInSuccess(data));
    } catch (error) {
      yield put(signInFailed());
      showError(error.response.data.errors);
    }
  }
}

export function* getCurrentUserSaga() {
  while (true) {
    yield take(UserActionTypes.USER_LOADED);
    try {
      const resp = yield call(getCurrentUser);
      const { data } = resp;
      yield put(loadUserSuccess(data));
    } catch (error) {
      yield put(loadUserFailed());
      showError(error.response.data.errors);
    }
  }
}

export function* updateUserSaga() {
  while (true) {
    const { payload } = yield take(UserActionTypes.UPDATE_USER);
    try {
      const resp = yield call(updateUser, payload);
      const { data } = resp;
      yield put(updateUserSuccess(data));
    } catch (error) {
      showError(error.response.data.errors);
    }
  }
}
