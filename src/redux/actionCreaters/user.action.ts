import {
  SignInFormValues,
  SignUpFormValues,
  UserAPIResponse,
  UserFormValues,
} from "../../models/user.model";
import { UserActionTypes } from "../actionTypes/types";

export const loadUserAction = () => {
  return {
    type: UserActionTypes.USER_LOADED,
    payload: {},
  };
};

export const loadUserSuccess = (data: UserAPIResponse) => {
  return {
    type: UserActionTypes.USER_LOADED_SUCCESS,
    payload: {
      currentUser: data.user,
    },
  };
};

export const loadUserFailed = () => {
  return {
    type: UserActionTypes.AUTH_ERROR,
  };
};

export const signInAction = (data: SignInFormValues) => {
  return {
    type: UserActionTypes.SIGNIN,
    payload: {
      user: data,
    },
  };
};

export const signInSuccess = (data: UserAPIResponse) => {
  return {
    type: UserActionTypes.SIGNIN_SUCCESS,
    payload: {
      currentUser: data.user,
    },
  };
};

export const signInFailed = () => {
  return {
    type: UserActionTypes.AUTH_ERROR,
  };
};

export const signUpAction = (data: SignUpFormValues) => {
  return {
    type: UserActionTypes.SIGNUP,
    payload: {
      user: data,
    },
  };
};

export const signUpSuccess = (data: UserAPIResponse) => {
  console.log(data);
  return {
    type: UserActionTypes.SIGNUP_SUCCESS,
    payload: {
      currentUser: data.user,
    },
  };
};

export const signUpFailed = () => {
  return {
    type: UserActionTypes.AUTH_ERROR,
  };
};

export const signOutAction = () => {
  return {
    type: UserActionTypes.SIGNOUT,
    payload: {},
  };
};

export const updateUser = (data: UserFormValues) => {
  return {
    type: UserActionTypes.UPDATE_USER,
    payload: data,
  };
};

export const updateUserSuccess = (data: UserAPIResponse) => {
  return {
    type: UserActionTypes.UPDATE_USER_SUCCESS,
    payload: data.user,
  };
};

export const updateUserFail = () => {
  return {
    type: UserActionTypes.UPDATE_USER_FAIL,
  };
};
