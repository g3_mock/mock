import { ProfileAPIResponse } from "../../models/profile.model";
import { ProfileActionTypes } from "../actionTypes/types";

export const getProfileAction = (username: string) => {
  return {
    type: ProfileActionTypes.GET_PROFILE,
    payload: username,
  };
};

export const getProfileSuccess = (data: ProfileAPIResponse) => {
  return {
    type: ProfileActionTypes.GET_PROFILE_SUCESS,
    payload: data.profile,
  };
};

export const getProfileFailed = () => {
  return {
    type: ProfileActionTypes.PROFILE_ERROR,
  };
};

export const followUser = (username: string) => {
  return {
    type: ProfileActionTypes.FOLLOW_USER,
    payload: username,
  };
};

export const followUserSuccess = (data: ProfileAPIResponse) => {
  return {
    type: ProfileActionTypes.FOLLOW_USER_SUCCESS,
    payload: data.profile,
  };
};

export const unfollowUser = (username: string) => {
  return {
    type: ProfileActionTypes.UNFOLLOW_USER,
    payload: username,
  };
};

export const unfollowUserSuccess = (data: ProfileAPIResponse) => {
  return {
    type: ProfileActionTypes.UNFOLLOW_USER_SUCCESS,
    payload: data.profile,
  };
};
