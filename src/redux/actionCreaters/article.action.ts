import {
  ArticleAPIResponse,
  ArticleFormValues,
  ArticlesAPIResponse,
  CommentAPIRequest,
  CommentAPIResponse,
  TagAPIResponse,
} from "../../models/article.model";
import { ProfileAPIResponse } from "../../models/profile.model";
import { ArticleActionTypes } from "../actionTypes/types";

//  REGION: Articles actions
export const getArticlesAction = (paramsString: string) => {
  return {
    type: ArticleActionTypes.GET_ARTICLES,
    payload: paramsString,
  };
};

export const getArticlesSuccess = (data: ArticlesAPIResponse) => {
  const { articles, articlesCount } = data;
  return {
    type: ArticleActionTypes.GET_ARTICLES_SUCCESS,
    payload: {
      articles,
      articlesCount,
    },
  };
};

export const getArticlesFailed = () => {
  return {
    type: ArticleActionTypes.ARTICLES_ERROR,
  };
};

export const getTagsAction = () => {
  return {
    type: ArticleActionTypes.GET_TAGS,
    payload: [],
  };
};

export const getTagsSuccess = (data: TagAPIResponse) => {
  return {
    type: ArticleActionTypes.GET_TAGS_SUCCESS,
    payload: data,
  };
};
// TODO: Hidden one Article
export const hiddenArticlePost = (slug: string) => {
  return {
    type: ArticleActionTypes.HIDDEN_ONE_ARTICLES_POST,
    payload: slug,
  };
};

// REGION: Detail article actions
export const getArticleAction = (slug: string) => {
  return {
    type: ArticleActionTypes.GET_ARTICLE,
    payload: slug,
  };
};

export const getArticleSuccess = (data: ArticleAPIResponse) => {
  const { article } = data;
  return {
    type: ArticleActionTypes.GET_ARTICLE_SUCCESS,
    payload: article,
  };
};

export const getArticleFailed = () => {
  return {
    type: ArticleActionTypes.GET_ARTICLE_ERROR,
  };
};

export const deleteArticleAction = (slug: string) => {
  return {
    type: ArticleActionTypes.DELETE_ARTICLE,
    payload: slug,
  };
};

export const undoArticleAction = () => {
  return {
    type: ArticleActionTypes.UNDO_ARTICLE,
  };
};

export const deleteArticleSuccess = (slug: string) => {
  return {
    type: ArticleActionTypes.DELETE_ARTICLE_SUCCESS,
    payload: slug,
  };
};
// TODO: Favorite article action
export const likeArticleAction = (slug: string) => {
  return {
    type: ArticleActionTypes.LIKE_ARTICLE,
    payload: slug,
  };
};

export const unLikeArticleAction = (slug: string) => {
  return {
    type: ArticleActionTypes.UNLIKE_ARTICLE,
    payload: slug,
  };
};

export const likeArticleSuccess = (data: ArticleAPIResponse) => {
  const { article } = data;
  return {
    type: ArticleActionTypes.LIKE_ARTICLE_SUCCESS,
    payload: article,
  };
};

export const unLikeArticleSuccess = (data: ArticleAPIResponse) => {
  const { article } = data;
  return {
    type: ArticleActionTypes.UNLIKE_ARTICLE_SUCCESS,
    payload: article,
  };
};

// TODO: get your feed

export const getArticlesFeed = (paramsString: string) => {
  return {
    type: ArticleActionTypes.GET_ARTICLE_FEED,
    payload: paramsString,
  };
};

export const getArticlesFeedSuccess = (data: ArticlesAPIResponse) => {
  const { articles, articlesCount } = data;
  return {
    type: ArticleActionTypes.GET_ARTICLE_FEED_SUCCESS,
    payload: { articles, articlesCount },
  };
};

export const getArticlesfeedFail = () => {
  return {
    type: ArticleActionTypes.ARTICLES_ERROR,
  };
};

// TODO: follow and unfollow
export const followUserArticle = (username: string) => {
  return {
    type: ArticleActionTypes.FOLLOW_USER_ARTICLE,
    payload: username,
  };
};

export const followUserArticleSuccess = (data: ProfileAPIResponse) => {
  return {
    type: ArticleActionTypes.FOLLOW_USER_ARTICLE_SUCCESS,
    payload: data.profile,
  };
};

export const unfollowUserArticleSuccess = (data: ProfileAPIResponse) => {
  return {
    type: ArticleActionTypes.UNFOLLOW_USER_ARTICLE_SUCCESS,
    payload: data.profile,
  };
};

export const unfollowUserArticle = (username: string) => {
  return {
    type: ArticleActionTypes.UNFOLLOW_USER_ARTICLE,
    payload: username,
  };
};

// TODO: Create and Update article
export const createArticle = (data: ArticleFormValues) => {
  return {
    type: ArticleActionTypes.CREATE_ARTICLE,
    payload: data,
  };
};

export const createArticleSuccess = (data: ArticleAPIResponse) => {
  const { article } = data;
  return {
    type: ArticleActionTypes.CREATE_ARTICLE_SUCCESS,
    payload: article,
  };
};

export const createArticleFail = () => {
  return {
    type: ArticleActionTypes.CREATE_ARTICLE_FAIL,
  };
};

export const updateArticle = (slug: string, data: ArticleFormValues) => {
  return {
    type: ArticleActionTypes.UPDATE_ARTICLE,
    payload: { slug, data },
  };
};

export const updateArticleSuccess = (data: ArticleAPIResponse) => {
  const { article } = data;
  return {
    type: ArticleActionTypes.UPDATE_ARTICLE_SUCCESS,
    payload: article,
  };
};

export const updateArticleFail = () => {
  return {
    type: ArticleActionTypes.UPDATE_ARTICLE_FAIL,
  };
};

// REGION: Comments actions

export const getCommentsSuccess = (data: CommentAPIResponse) => {
  const { comments } = data;
  return {
    type: ArticleActionTypes.GET_COMMENTS_SUCCESS,
    payload: comments,
  };
};

export const getCommentsFail = () => {
  return {
    type: ArticleActionTypes.GET_COMMENTS_FAIL,
  };
};

export const addCommentAction = (slug: string, data: CommentAPIRequest) => {
  return {
    type: ArticleActionTypes.ADD_COMMENT,
    payload: {
      slug,
      data,
    },
  };
};

export const addCommentSuccess = (data: Comment) => {
  return {
    type: ArticleActionTypes.ADD_COMMENT_SUCCESS,
    payload: data,
  };
};

export const deleteCommentAction = (slug: string, id: number) => {
  return {
    type: ArticleActionTypes.DELETE_COMMENT,
    payload: {
      slug,
      id,
    },
  };
};

export const deleteCommentSuccess = (id: number) => {
  return {
    type: ArticleActionTypes.DELETE_COMMENT_SUCCESS,
    payload: id,
  };
};
