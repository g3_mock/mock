import { ActionType, ArticleState } from "../../models/redux.model";
import { ArticleActionTypes } from "../actionTypes/types";

const initialState: ArticleState = {
  articles: [],
  articlesCount: 0,
  article: null,
  loading: true,
  tags: [],
  comments: [],
  loadingComments: false,
  error: false,
  navigateAfterCreateOrUpdateArticle: false,
  openUndo: false,
};

function articleReducer(state = initialState, action: ActionType) {
  const { type, payload } = action;

  switch (type) {
    case ArticleActionTypes.GET_ARTICLE:
      return {
        ...state,
        comments: [],
        loading: true,
        loadingComments: true,
      };
    case ArticleActionTypes.GET_ARTICLES:
    case ArticleActionTypes.GET_ARTICLE_FEED:
      return {
        ...state,
        loading: true,
      };
    case ArticleActionTypes.GET_ARTICLES_SUCCESS:
    case ArticleActionTypes.GET_ARTICLE_FEED_SUCCESS:
    case ArticleActionTypes.GET_TAGS_SUCCESS:
      return {
        ...state,
        ...payload,
        loading: false,
        error: false,
      };
    case ArticleActionTypes.GET_ARTICLE_SUCCESS:
      return {
        ...state,
        article: payload,
        loading: false,
        error: false,
      };
    case ArticleActionTypes.DELETE_ARTICLE:
      return {
        ...state,
        openUndo: true,
      };
    case ArticleActionTypes.DELETE_ARTICLE_SUCCESS:
      return {
        ...state,
        articles: state.articles.filter((artile) => artile.slug !== payload),
        loading: false,
        openUndo: false,
      };
    case ArticleActionTypes.UNDO_ARTICLE:
      return {
        ...state,
        openUndo: false,
      };
    case ArticleActionTypes.GET_ARTICLE_ERROR:
      return {
        ...state,
        article: null,
        loading: false,
        error: true,
      };
    case ArticleActionTypes.ARTICLES_ERROR:
      return {
        ...state,
        articles: [],
        loadingComments: false,
      };
    case ArticleActionTypes.CREATE_ARTICLE_SUCCESS:
      return {
        ...state,
        articles: [payload, ...state.articles],
        navigateAfterCreateOrUpdateArticle: true,
      };
    case ArticleActionTypes.CREATE_ARTICLE:
    case ArticleActionTypes.CREATE_ARTICLE_FAIL:
    case ArticleActionTypes.UPDATE_ARTICLE:
    case ArticleActionTypes.UPDATE_ARTICLE_FAIL:
      return {
        ...state,
        navigateAfterCreateOrUpdateArticle: false,
      };
    case ArticleActionTypes.UPDATE_ARTICLE_SUCCESS:
      return {
        ...state,
        article: payload,
        articles: state.articles.map((article) =>
          article.slug === payload.slug ? payload : article
        ),
        navigateAfterCreateOrUpdateArticle: true,
      };
    case ArticleActionTypes.GET_COMMENTS_SUCCESS:
      return {
        ...state,
        loadingComments: false,
        comments: payload,
        navigateAfterCreateOrUpdateArticle: false,
      };
    case ArticleActionTypes.GET_COMMENTS_FAIL:
      return {
        ...state,
        loadingComments: false,
      };
    case ArticleActionTypes.ADD_COMMENT_SUCCESS:
      return {
        ...state,
        comments: [payload, ...state.comments],
      };
    case ArticleActionTypes.DELETE_COMMENT_SUCCESS:
      return {
        ...state,
        comments: state.comments.filter((comment) => comment.id !== payload),
      };
    case ArticleActionTypes.LIKE_ARTICLE_SUCCESS:
    case ArticleActionTypes.UNLIKE_ARTICLE_SUCCESS:
      return {
        ...state,
        article: payload,
        articles: state.articles.map((article) =>
          article.slug === payload.slug ? payload : article
        ),
      };
    case ArticleActionTypes.ADD_COMMENT:
    case ArticleActionTypes.DELETE_COMMENT:
    case ArticleActionTypes.LIKE_ARTICLE:
    case ArticleActionTypes.UNLIKE_ARTICLE:
    case ArticleActionTypes.FOLLOW_USER_ARTICLE:
    case ArticleActionTypes.UNFOLLOW_USER_ARTICLE:
      return {
        ...state,
      };
    case ArticleActionTypes.FOLLOW_USER_ARTICLE_SUCCESS:
    case ArticleActionTypes.UNFOLLOW_USER_ARTICLE_SUCCESS:
      return {
        ...state,
        article: { ...state.article, author: payload },
      };
    case ArticleActionTypes.HIDDEN_ONE_ARTICLES_POST:
      return {
        ...state,
        articles: state.articles.filter((article) => article.slug !== payload),
      };
    default:
      return state;
  }
}

export default articleReducer;
