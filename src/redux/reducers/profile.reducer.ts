import { ActionType, ProfileState } from "../../models/redux.model";
import { ProfileActionTypes } from "../actionTypes/types";

const initialState: ProfileState = {
  profile: null,
  loading: true,
  error: false,
};

function profileReducer(state = initialState, action: ActionType) {
  const { type, payload } = action;

  switch (type) {
    case ProfileActionTypes.GET_PROFILE:
      return {
        ...state,
        loading: true,
      };
    case ProfileActionTypes.GET_PROFILE_SUCESS:
      return {
        ...state,
        profile: payload,
        loading: false,
        error: false,
      };
    case ProfileActionTypes.PROFILE_ERROR:
      return {
        ...state,
        profile: null,
        error: true,
        loading: false,
      };
    case ProfileActionTypes.FOLLOW_USER:
      return {
        ...state,
      };
    case ProfileActionTypes.FOLLOW_USER_SUCCESS:
      return {
        ...state,
        profile: payload,
      };
    case ProfileActionTypes.UNFOLLOW_USER:
      return {
        ...state,
      };
    case ProfileActionTypes.UNFOLLOW_USER_SUCCESS:
      return {
        ...state,
        profile: payload,
      };
    default:
      return state;
  }
}
export default profileReducer;
