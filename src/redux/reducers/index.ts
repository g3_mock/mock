import { combineReducers } from "redux";
import user from "./user.reducer";
import article from "./article.reducer";
import profile from "./profile.reducer";

const rootReducer = combineReducers({
  user,
  article,
  profile,
});

export default rootReducer;


