import { UserActionTypes } from "../actionTypes/types";
import { ActionType, UserState } from "../../models/redux.model";

const initialState: UserState = {
  token: null,
  isAuthenticated: false,
  currentUser: null,
  loading: true, //loading user when refresh
  authLoading: false, //loading when signin signup
  errorAuth: false,
};

function userReducer(state = initialState, action: ActionType) {
  const { type, payload } = action;

  switch (type) {
    case UserActionTypes.SIGNIN:
    case UserActionTypes.SIGNUP:
      return {
        ...state,
        authLoading: true,
        errorAuth: false,
      };
    case UserActionTypes.USER_LOADED_SUCCESS:
    case UserActionTypes.SIGNUP_SUCCESS:
    case UserActionTypes.SIGNIN_SUCCESS:
      return {
        ...state,
        ...payload,
        token: payload.currentUser.token,
        isAuthenticated: true,
        loading: false,
        authLoading: false,
        errorAuth: false,
      };
    case UserActionTypes.AUTH_ERROR:
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        loading: false,
        authLoading: false,
        currentUser: null,
        errorAuth: true,
      };
    case UserActionTypes.SIGNOUT:
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        loading: false,
        authLoading: false,
        currentUser: null,
        errorAuth: false,
      };
    case UserActionTypes.UPDATE_USER:
      return {
        ...state,
      };
    case UserActionTypes.UPDATE_USER_SUCCESS:
      return {
        ...state,
        currentUser: payload,
      };
    case UserActionTypes.UPDATE_USER_FAIL:
      return {
        ...state,
      };
    default:
      return state;
  }
}

export default userReducer;
