import Swal from "sweetalert2";
import { undoArticleAction } from "../redux/actionCreaters/article.action";

export const showError = (errors: Object) => {
  if (errors) {
    let errorStr = "";
    Object.entries(errors).forEach((item) => {
      errorStr = item.join(" ");
    });
    Swal.fire(`${errorStr}`, "", "error");
  }
};

export const showNotifySuccess = (mes: string) => {
  Swal.fire(`${mes}`, "Success", "success");
};

export const undoNotifyDeletedArticle = (dispatch: any) => {
  Swal.fire({
    title: "Deleted article success!",
    text: "You have 3s to undo!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Undo delete",
    cancelButtonText: "Cancel",
    timer: 3000,
    timerProgressBar: true,
  }).then((result) => {
    if (result.isConfirmed) {
      dispatch(undoArticleAction());
    }
  });
};
