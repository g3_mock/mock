import React from "react";
import { makeStyles, createStyles } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";

interface Props {
  pagination: {
    page: number;
    limit: number;
    totalRows: number;
  };
  onPageChange: (newPage: number) => void;
}

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      "& > *": {
        marginTop: theme.spacing(2),
      },
    },
  })
);

export default function PaginationComponent({
  pagination,
  onPageChange,
}: Props) {
  const classes = useStyles();
  const { limit, totalRows } = pagination;
  const totalPages = Math.ceil(totalRows / limit);

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    onPageChange(value);
  };

  return (
    <div className={classes.root}>
      <Pagination
        count={totalPages}
        variant="outlined"
        onChange={handleChange}
        color="primary"
      />
    </div>
  );
}
