import React from "react";
import { useSelector } from "react-redux";
import {
  Redirect,
  Route,
  RouteChildrenProps,
  RouteComponentProps,
  RouteProps,
  withRouter,
} from "react-router-dom";

interface PrivateRouteProps {
  component?: React.FC;
  children?:
    | ((props: RouteChildrenProps<any>) => React.ReactNode)
    | React.ReactNode;
}

function PrivateRoute({
  component: Component,
  children,
  ...rest
}: PrivateRouteProps & RouteProps & RouteComponentProps) {
  const { isAuthenticated, loading } = useSelector((state: any) => state.user);

  return (
    <Route
      {...rest}
      render={(props) =>
        !isAuthenticated && !loading ? (
          <Redirect to="/login" />
        ) : isAuthenticated ? (
          Component ? (
            <Component {...props} />
          ) : (
            children
          )
        ) : null
      }
    />
  );
}

export default withRouter(PrivateRoute);
