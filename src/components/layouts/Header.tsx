import {
  Avatar,
  Button,
  createStyles,
  Divider,
  IconButton,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  Menu,
  MenuItem,
  MenuProps,
  Paper,
  Theme,
  withStyles,
} from "@material-ui/core";
import {
  ArrowDropDown,
  ExitToApp,
  Home,
  Person,
  PersonAdd,
  Settings,
} from "@material-ui/icons";
import React, { useState } from "react";
import { Navbar } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { RootState } from "../../models/redux.model";
import { signOutAction } from "../../redux/actionCreaters/user.action";
import styles from "./header.module.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      borderRadius: 0,
    },
    menu: {
      display: "block",
      marginLeft: "auto",
    },
    small: {
      width: theme.spacing(4),
      height: theme.spacing(4),
    },
    medium: {
      width: theme.spacing(4.5),
      height: theme.spacing(4.5),
    },
    large: {
      width: theme.spacing(6),
      height: theme.spacing(6),
    },
    bgGrey: {
      backgroundColor: "#F3F3F3",
      marginLeft: 10,
    },
    chip: {
      backgroundColor: "#FFFFFF",
      borderRadius: 25,
      padding: 5,
      textTransform: "none",
    },
    paper: {
      borderRadius: "0px",
    },
  })
);

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
    paddingLeft: 10,
    paddingRight: 10,
    width: 300,
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    "&:focus": {
      marginTop: 3,
      marginBottom: 3,
    },
  },
}))(MenuItem);

const Header = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { currentUser } = useSelector((state: RootState) => state.user);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [guestDropdown, setGuestDropdown] = useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    currentUser
      ? setAnchorEl(event.currentTarget)
      : setGuestDropdown(event.currentTarget);
  };

  const handleClose = () => {
    currentUser ? setAnchorEl(null) : setGuestDropdown(null);
  };

  const handleSignOut = () => {
    dispatch(signOutAction());
    history.push("/login");
  };
  return (
    <Paper className={`${classes.paper} ${styles.header_shadow}`}>
      <>
        <Navbar
          variant="dark"
          className={`${classes.root} ${styles.header_shadow}`}
        >
          <NavLink className={`${styles.brand} navbar-brand`} to="/">
            ThanhHHien
          </NavLink>
          {currentUser ? (
            <div className={classes.menu}>
              <Button
                className={classes.chip}
                onClick={() => history.push(`/${currentUser.username}`)}
                startIcon={
                  <Avatar
                    className={classes.medium}
                    alt="Avatar"
                    src={currentUser.image}
                  />
                }
              >
                {currentUser.username}
              </Button>
              <IconButton
                className={classes.bgGrey}
                aria-label="Home"
                component="span"
                onClick={() => history.push("/")}
              >
                <Home fontSize="default" />
              </IconButton>
              <IconButton
                className={classes.bgGrey}
                aria-label="Settings"
                component="span"
                onClick={() => history.push("/settings")}
              >
                <Settings fontSize="small" />
              </IconButton>
              <IconButton
                className={classes.bgGrey}
                aria-label="Arrow DropDown"
                component="span"
                onClick={handleClick}
              >
                <ArrowDropDown fontSize="small" />
              </IconButton>
            </div>
          ) : (
            <div className={classes.menu}>
              <Button
                className={classes.chip}
                onClick={handleClick}
                startIcon={<Person fontSize="small" />}
                endIcon={<ArrowDropDown fontSize="default" />}
              >
                Accounts
              </Button>
            </div>
          )}
        </Navbar>
        {/* isAuthentication */}
        {currentUser ? (
          <StyledMenu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <StyledMenuItem
              onClick={() => {
                handleClose();
                history.push(`/${currentUser.username}`);
              }}
            >
              <ListItemAvatar>
                <Avatar className={classes.large} src={currentUser.image} />
              </ListItemAvatar>
              <ListItemText
                primary={currentUser.username}
                secondary={"see your profile"}
              />
            </StyledMenuItem>
            <Divider />
            <StyledMenuItem
              onClick={() => {
                handleClose();
                history.push(`/settings`);
              }}
            >
              <ListItemAvatar>
                <Avatar className={classes.small}>
                  <Settings />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Account Settings" />
            </StyledMenuItem>
            <StyledMenuItem
              onClick={() => {
                handleClose();
                handleSignOut();
              }}
            >
              <ListItemAvatar>
                <Avatar className={classes.small}>
                  <ExitToApp />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Sign out" />
            </StyledMenuItem>
          </StyledMenu>
        ) : (
          <StyledMenu
            id="simple-menu"
            anchorEl={guestDropdown}
            keepMounted
            open={Boolean(guestDropdown)}
            onClose={handleClose}
          >
            <StyledMenuItem
              onClick={() => {
                handleClose();
                history.push("/login");
              }}
            >
              <ListItemAvatar>
                <Avatar className={classes.small}>
                  <Person />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Sign in" />
            </StyledMenuItem>
            <StyledMenuItem
              onClick={() => {
                handleClose();
                history.push("/register");
              }}
            >
              <ListItemAvatar>
                <Avatar className={classes.small}>
                  <PersonAdd />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Sign up" />
            </StyledMenuItem>
          </StyledMenu>
        )}
      </>
    </Paper>
  );
};

export default Header;
