import { Button } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
const NotFound = () => {
  const history = useHistory();

  return (
    <div className="text-center mt-5">
      <h1 className="pt-5">404</h1>
      <h2 className="mb-0">The requested page dose not exist!</h2>
      <Button
        onClick={() => {
          history.push("/");
        }}
        variant="contained"
        color="primary"
        className="mt-5"
      >
        Back to home
      </Button>
    </div>
  );
};

export default NotFound;
