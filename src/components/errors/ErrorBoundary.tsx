import { Button } from "@material-ui/core";
import React, { ReactChild } from "react";
import imgSrc from "../../assets/errorpage.jpg";

interface Props {
  children: ReactChild;
}
const rootStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  backgroundImage: `url(${imgSrc})`,
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center center",
  backgroundAttachment: "fixed",
  backgroundSize: "auto",
  height: "100vh",
};

class ErrorBoundary extends React.Component<Props> {
  state = { hasError: false };

  static getDerivedStateFromError(error: Error) {
    return { hasError: true };
  }
  componentDidCatch(error: Error, info: object) {
    console.log(error, info);
  }
  render() {
    if (this.state.hasError) {
      return (
        <div style={rootStyle}>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => window.location.reload()}
          >
            Back to Home
          </Button>
        </div>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
