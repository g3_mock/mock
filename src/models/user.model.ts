export interface UserFormValues {
  email: string;
  username: string;
  password: string;
  image: string | undefined;
  bio: string | undefined;
}

export interface SignUpFormValues {
  username: string;
  email: string;
  password: string;
}

export interface SignInFormValues {
  email: string;
  password: string;
}

export interface User {
  id: number;
  email: string;
  createdAt: string;
  updatedAt: string;
  username: string;
  bio?: string;
  image?: string;
  token: string;
}

export interface UserAPIResponse {
  user: User;
}

export interface User {
  id: number;
  email: string;
  createdAt: string;
  updatedAt: string;
  username: string;
  bio?: string;
  image?: string;
  token: string;
}
