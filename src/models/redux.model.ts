import { Article, Comment } from "./article.model";
import { Profile } from "./profile.model";

export interface ActionType {
  type: string;
  payload: any;
}

export interface UserState {
  token?: any;
  isAuthenticated: boolean;
  currentUser?: any;
  loading: boolean;
  authLoading: boolean;
  errorAuth: boolean;
}

export interface ProfileState {
  profile: Profile | null;
  loading: boolean;
  error: boolean;
}

export interface ArticleState {
  articles: Article[];
  articlesCount: number;
  article: Article | null;
  loading: boolean;
  tags: string[];
  comments: Comment[];
  loadingComments: boolean;
  error: boolean;
  navigateAfterCreateOrUpdateArticle: boolean;
  openUndo: boolean;
}

export interface RootState {
  user: UserState;
  profile: ProfileState;
  article: ArticleState;
}
