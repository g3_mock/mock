export interface Profile {
  username: string;
  bio?: any;
  image: string;
  following: boolean;
}
export interface ProfileAPIResponse {
  profile: Profile;
}
