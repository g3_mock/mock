export interface Article {
  title: string;
  slug: string;
  body: string;
  createdAt: string;
  updatedAt: string;
  tagList: string[];
  description: string;
  author: Author;
  favorited: boolean;
  favoritesCount: number;
}

export interface ArticleFormValues {
  title: string;
  description: string;
  body: string;
  tagList: string[];
}

interface Author {
  username: string;
  bio?: any;
  image: string;
  following: boolean;
}

export interface ArticlesAPIResponse {
  articles: Article[];
  articlesCount: number;
}

export interface ArticleAPIResponse {
  article: Article;
}

export interface TagAPIResponse {
  tags: string[];
}

// TODO: Comment Api Response Type
export interface CommentAPIResponse {
  comments: Comment[];
}

export interface Comment {
  id: number;
  createdAt: string;
  updatedAt: string;
  body: string;
  author: {
    username: string;
    bio: string;
    image: string;
    following: boolean;
  };
}

export interface CommentAPIRequest {
  comment: {
    body: string;
  };
}
