import React, { useEffect } from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { setAuthToken } from "./apis/api";
import AppRoute from "./AppRoute";
import ErrorBoundary from "./components/errors/ErrorBoundary";
import Footer from "./components/layouts/Footer";
import Header from "./components/layouts/Header";
import {
  loadUserAction,
  signOutAction,
} from "./redux/actionCreaters/user.action";
import store from "./redux/store";

const App = () => {
  useEffect(() => {
    // check for token in LS
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }
    store.dispatch(loadUserAction());

    // log user out from all tabs if they log out in one tab
    window.addEventListener("storage", () => {
      if (!localStorage.token) store.dispatch(signOutAction());
    });
  }, []);

  return (
    <ErrorBoundary>
      <Provider store={store}>
        <BrowserRouter>
          <Header />
          <AppRoute />
          <Footer />
        </BrowserRouter>
      </Provider>
    </ErrorBoundary>
  );
};

export default App;
